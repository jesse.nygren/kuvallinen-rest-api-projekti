CREATE DATABASE rajapintaprojekti;

CREATE TABLE Pictures(
    ID integer NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(50),
    description varchar(500),
    tags varchar(500),
    filepath varchar(100),
    likes integer
);

INSERT INTO Pictures (name, description, tags, filepath, likes) 
    VALUES ("Kuva1", "Hassu kuva haha", "#FUNNY","0.jpg", 0);
INSERT INTO Pictures (name, description, tags, filepath, likes) 
    VALUES ("Kuva2", "Kuva jostain jutusta", "#lol","1.jpg", 0);
INSERT INTO Pictures (name, description, tags, filepath, likes) 
    VALUES ("Kuva3", "hehehe", "#juu","2.jpg", 0);
