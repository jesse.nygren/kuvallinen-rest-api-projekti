var express = require('express');
var multer = require('multer');
var bodyParser = require('body-parser')
var mysql = require('mysql');
var fs = require('fs');
var path = require('path');
var process = require("process");
var app = express();

app.use(bodyParser.json())
app.use(express.static("./"))
let filename;

// Luodaan varasto-olio lisättävälle kuvatiedostolle. Kuvien sijainti ./public/images/
var storage = multer.diskStorage({
    destination: function(req, file, callback){
        callback(null, './public/images')
    },
    filename: function(req, file, callback){
        filename = file.fieldname + '-' + Date.now() + ".jpg"
        console.log("Image: " + filename + " uploaded.")
        callback(null, filename);
    }
});

var upload = multer({storage : storage},{limits : {fieldNameSize : 10}}).single('photo');

// Luodaan MySQL tietokantayhteys
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    //password: "password",
    database: "rajapintaprojekti",
    insecureAuth : true
  });

app.get('/',function(req,res){
    res.sendFile(__dirname + "/index.html");
});

// Metodi luo kuvien "GET":taamiseen palautettavan JSONin
function parseToJson(result){
    var response = '{"pictures":[ '
    for (var i = 0, len = result.length; i < len; i++) {
            console.log(result[i].id);
        if (i==(len-1)) {
            response += '{"id": "'+result[i].id+'","name": "'+result[i].name+'", "description":"'+result[i].description+'","tags":"'+result[i].tags+'","filepath":"'+result[i].filepath+'",';
            response += '"likes": "'+result[i].likes+'"}]}';
            
        }
        else{
            response += '{"id": "'+result[i].id+'","name": "'+result[i].name+'", "description":"'+result[i].description+'","tags":"'+result[i].tags+'","filepath":"'+result[i].filepath+'",';
            response += '"likes": "'+result[i].likes+'"},';

        }
    }
    return response;
}

// GET
// Kaikkien kuvien haku palvelimelta
app.get('/api/all_images', function(req, res){
        con.query('SELECT * FROM pictures', function (err, result, fields) {
        if (err) throw err;
            //console.log(result);
        var response = '{"pictures":[ '
        for (var i = 0, len = result.length; i < len; i++) {
                console.log(result[i].id);
            if (i==(len-1)) {
                response += '{"id": "'+result[i].id+'","name": "'+result[i].name+'", "description":"'+result[i].description+'","tags":"'+result[i].tags+'","filepath":"'+result[i].filepath+'",';
                response += '"likes": "'+result[i].likes+'"}]}';
                
            }
            else{
                response += '{"id": "'+result[i].id+'","name": "'+result[i].name+'", "description":"'+result[i].description+'","tags":"'+result[i].tags+'","filepath":"'+result[i].filepath+'",';
                response += '"likes": "'+result[i].likes+'"},';

            }
        }
            console.log(result);

    res.end(response)
        });
});

// DELETE
// Poistaa nimetyn kuvan tietokannasta, mutta jättää kuvatiedoston palvelimelle.
app.delete('/api/deleteImage/:name', function(req, res){
    var name = req.params.name;
    console.log("ID: " + name)
    con.query('DELETE from pictures where name = "' + name + '"', function (err, result, fields) {
        if(err){
            let jsonResponse = '{"message" : "'+err+'"}';
            return res.end(JSON.stringify(jsonResponse));
        }

        let jsonResponse = '{"message" : "Image" +name+ "Removed"}';
        res.end(JSON.stringify(jsonResponse));
        console.log(result);
        });

});

// GET
// Hakee yhden kuvan nimellä
app.get('/api/image/:name', function(req, res){
    var name = req.params.name;
    console.log("ID: " + name)
    con.query('SELECT * from pictures where name = "' + name+'"', function (err, result, fields) {
        if(err){
            let jsonResponse = '{"message" : "'+err+'"}';
            return res.end(JSON.stringify(jsonResponse));
        }

        res.end(parseToJson(result))
        console.log(parseToJson(result))
        console.log(result);
        });

});

// POST
// Metodi kuvien tykkäämiseen. Kuva valitaan ID:llä.
app.post('/api/like/:id', function(req, res){
            const id = parseInt(req.params.id, 10);
            console.log(id);
            con.query('UPDATE pictures SET likes=likes+1 where id ='+id, function (err, result, fields) {
                if(err){
                    let jsonResponse = '{"message" : "'+err+'"}';
                    return res.end(JSON.stringify(jsonResponse));
                }
            let jsonResponse = '{"message" : "Liked Image"}';
            res.end(JSON.stringify(jsonResponse));
            });
});

// POST
// Metodi kuvien ei-tykkäykseen. Kuva valitaan ID:llä.
app.post('/api/dislike/:id', function(req, res){
            const id = parseInt(req.params.id, 10);
            console.log(id);
            con.query('UPDATE pictures SET likes=likes-1 where id = '+id, function (err, result, fields) {
                if(err){
                    let jsonResponse = '{"message" : "'+err+'"}';
                    return res.end(JSON.stringify(jsonResponse));
                }
                let jsonResponse = '{"message" : "Image Updated"}';
                res.end(JSON.stringify(jsonResponse));
            });
});

// POST
// Kuvan lisääminen palvelimelle. Kuvatiedoston lisäksi tulee: nimi, kuvaus ja tunnisteet.
app.post('/api/image', function(req, res){

    console.log(req)
    upload(req, res, function(err){
        if(err){
            let jsonResponse = '{"message" : "'+err+'"}';
            return res.end(JSON.stringify(jsonResponse));
        }

        if (req.body.name && req.body.description && req.body.tags){
        con.connect(function(err) {
            if(err){
                let jsonResponse = '{"message" : "'+err+'"}';
                return res.end(JSON.stringify(jsonResponse));
            }
            con.query('INSERT INTO pictures (name, description, tags, filepath, likes) VALUES ("'+req.body.name.toString()+'", "'+req.body.description.toString()+'","'+ req.body.tags.toString()+'",' + '"./public/images/' + filename +'", "0")', function (err, result, fields) {
            console.log(result);
            });
        });
    } else {
        return res.end("Upload error. Name/description/tags missing. "+err)
    }
        let jsonResponse = '{"message" : "Image Added"}';
        res.end(JSON.stringify(jsonResponse));
    })
});

// POST 
// Kuvan kuvauksen ja tunnisteiden muuttaminen. Kuvan nimeä ja kuvatiedostoa ei voi muuttaa.
app.post('/api/updateImage', function(req, res){
            const name = req.body.name.toString();
            const desc = req.body.description.toString();
            const tags = req.body.tags.toString();
            //console.log(name);
            con.query('UPDATE pictures SET description="'+desc+'", tags="'+tags+'"where name = "'+name+'"', function (err, result, fields) {
                if(err){
                    let jsonResponse = '{"message" : "'+err+'"}';
                    return res.end(JSON.stringify(jsonResponse));
                }
                let jsonResponse = '{"message" : "Image Updated"}';
                res.end(JSON.stringify(jsonResponse));
            });

});

var server = app.listen(8081, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Kuva-app at http:/localhost:", host, port)
});