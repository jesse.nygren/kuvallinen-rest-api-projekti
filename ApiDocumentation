Huy Nguyen, Jere Moilanen, Jori Hänninen, Jesse Nygren 

- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Kuvapalvelu REST API

API DOKUMENTAATIO
- - - - - - - - - - - - - - - - - - - - - - - - - - - -

Kaikkien kuvien hakeminen

GET

curl		: http://<server-address>:<server-port>/api/all_images

Response	: 

{
"pictures":[
	"id": "1",
	"name": "Kuva", 
	"description":"Tämä on kuva",
	"tags":"kuva,hauska,jee",
	"filepath":"./kuvat/kuva.jpg",
	"likes": ""
	]
}

Error response: 

{
	"message" : "<Error message>"
}
- - - - - - - - - - - - - - - - - - - - - - - - - - - -

Kuvan hakeminen nimellä

GET

curl		: http://<server-address>:<server-port>/api/image/:name

Parameter	: /:name
Esimerkki	: http://<server-address>:<server-port>/api/image/hauskakuva

Response	: 

{
"pictures":[
	"id": "1",
	"name": "Kuva", 
	"description":"Tämä on kuva",
	"tags":"kuva,hauska,jee",
	"filepath":"./kuvat/kuva.jpg",
	"likes": ""
	]
}

Error response: 

{
	"message" : "<Error message>"
}

- - - - - - - - - - - - - - - - - - - - - - - - - - - -

Kuvan poistaminen nimellä

DELETE

curl		: http://<server-address>:<server-port>/api/deleteImage/:name

Parameter	: /:name
Esimerkki	: http://<server-address>:<server-port>/api/image/hauskakuva

Response	: 

{
"message":"Image removed"
}

Error response: 

{
	"message" : "<Error message>"
}
- - - - - - - - - - - - - - - - - - - - - - - - - - - -

Kuvan lisääminen

POST

curl		: http://<server-address>:<server-port>/api/image

POST JSON	: 

{
"name":"nimi",
"description":"kuvaus",
"tags":"tagit",
"photo":<kuvatiedosto>
}

Content-Type: multipart/form-data

Response	: 

{
"message":"Image uploaded"
}

Error response: 

{
	"message" : "<Error message>"
}
- - - - - - - - - - - - - - - - - - - - - - - - - - - -

Liken ("Tykkää") lisääminen kuvaan id:llä

POST

curl		: http://<server-address>:<server-port>/api/like/:id

Parameter	: /:id, kuvan id
Esimerkki	: http://<server-address>:<server-port>/api/like/1
		
POST JSON	: 

{
"name":"kuvan nimi"
}		
	
Content-Type	: application/json

Response	: 

{
"message":"Liked image"
}

Error response: 

{
	"message" : "<Error message>"
}
- - - - - - - - - - - - - - - - - - - - - - - - - - - -

Disliken ("Ei tykkää") lisääminen kuvaan id:llä

POST

curl		: http://<server-address>:<server-port>/api/dislike/:id

Parameter	: /:id, kuvan id
Esimerkki	: http://<server-address>:<server-port>/api/like/1
		
POST JSON	: 

{
"name":"kuvan nimi"
}		

Content-Type	: application/json

Response	: 

{
"message":"Disliked image"
}

Error response: 

{
	"message" : "<Error message>"
}
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
Kuvan tietojen päivittäminen

POST

curl		: http://<server-address>:<server-port>/api/image/:name

Parameter	: /:name, kuvan nimi
Esimerkki	: http://<server-address>:<server-port>/api/image/hauskakuva

POST JSON	:

{
"name":"nimi",
"description":"kuvaus",
"tags":"tagit"
}

Content-Type	: application/json

Response	: 

{
"message":"Image updated"
}

Error response: 

{
	"message" : "<Error message>"
}
- - - - - - - - - - - - - - - - - - - - - - - - - - - -
